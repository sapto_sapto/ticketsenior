package com.sapto.example.mongo.flux.mongoflux.endpoint;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sapto.example.mongo.flux.mongoflux.customer.persistence.Customer;
import com.sapto.example.mongo.flux.mongoflux.customer.repository.CustomerRepository;
import com.sapto.example.mongo.flux.mongoflux.employee.persistence.Employee;
import com.sapto.example.mongo.flux.mongoflux.employee.repository.EmployeeRepository;
import com.sapto.example.mongo.flux.mongoflux.order.persistence.OrderDetail;
import com.sapto.example.mongo.flux.mongoflux.order.persistence.OrdersManaged;
import com.sapto.example.mongo.flux.mongoflux.order.repository.OrderDetailRepository;
import com.sapto.example.mongo.flux.mongoflux.order.services.OrderService;
import com.sapto.example.mongo.flux.mongoflux.product.persistence.Product;
import com.sapto.example.mongo.flux.mongoflux.product.repository.ProductRepository;
import com.sapto.example.mongo.flux.mongoflux.services.DataProcessor;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class EcommerceAPI {

	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private OrderDetailRepository orderDetailRepository;

	@RequestMapping(value = "/save/customer", method = RequestMethod.POST)
	public ResponseEntity<String> insertDataCustomer(@RequestBody Customer customer) {

		customerRepository.save(customer);
		return new ResponseEntity<>("OK", HttpStatus.OK);
	}

	@RequestMapping(value = "/save/employee", method = RequestMethod.POST)
	public ResponseEntity<String> insertDataEmployee(@RequestBody Employee employee) {

		employeeRepository.save(employee);
		return new ResponseEntity<>("OK", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/save/product", method = RequestMethod.POST)
	public ResponseEntity<String> insertDataProduct(@RequestBody Product product) {

		productRepository.save(product);
		return new ResponseEntity<>("OK", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/save/orderDetail", method = RequestMethod.POST)
	public ResponseEntity<String> insertDataOrderDetail(@RequestBody OrderDetail orderDetail) {

		orderDetailRepository.save(orderDetail);
		return new ResponseEntity<>("OK", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findById/orderDetail/{id}", method = RequestMethod.GET)
	public ResponseEntity<OrderDetail> findOrderDetail(@PathVariable("id") Integer id) {

		 OrderDetail orderDetail = orderDetailRepository.findById(id).orElse(null);
		return new ResponseEntity<>(orderDetail, HttpStatus.OK);
	}
	
	@Autowired
	private DataProcessor csvProcessor;

	@Qualifier("orderServiceRDBMS")
	@Autowired
	private OrderService orderService;

	@RequestMapping(value = "/import/employee", method = RequestMethod.GET)
	@ApiOperation(value = "Import Data Employee from CSV")
	public ResponseEntity<String> importEmployee(HttpServletRequest request) {

		String response = csvProcessor.processEmployee();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/import/product", method = RequestMethod.GET)
	@ApiOperation(value = "Import Data Employee from CSV")
	public ResponseEntity<String> importProduct(HttpServletRequest request) {

		String response = csvProcessor.processProduct();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/import/customer", method = RequestMethod.GET)
	@ApiOperation(value = "Import Data Customer from CSV")
	public ResponseEntity<String> importCustomer(HttpServletRequest request) {

		String response = csvProcessor.processCustomer();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/import/shippingMethod", method = RequestMethod.GET)
	@ApiOperation(value = "Import Data Shipping Method from CSV")
	public ResponseEntity<String> importShippingMethod(HttpServletRequest request) {

		String response = csvProcessor.processShippingMethod();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/import/order", method = RequestMethod.GET)
	@ApiOperation(value = "Import Data Order from CSV")
	public ResponseEntity<String> importOrder(HttpServletRequest request) {

		String response = csvProcessor.processOrders();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/import/orderDetail", method = RequestMethod.GET)
	@ApiOperation(value = "Import Data Order Detail from CSV")
	public ResponseEntity<String> importOrderDetail(HttpServletRequest request) {

		String response = csvProcessor.processOderDetail();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/findAll/order/{page}/{size}", method = RequestMethod.GET)
	@ApiOperation(value = "Find Data Order by Page (a,b)")
	public Page<OrdersManaged> findAllOrder(HttpServletRequest request, @PathVariable("page") Integer page,
			@PathVariable("size") Integer size) {

		Page<OrdersManaged> orders = orderService.findAllOrder(PageRequest.of(page, size));

		return orders;
	}

}
