package com.sapto.example.mongo.flux.mongoflux.persistence;

public interface IBaseUser {
	
	public String getLastName();

	public void setLastName(String lastName);
	
	public String getFirstName();

	public void setFirstName(String firstName);

}
