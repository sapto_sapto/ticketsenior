package com.sapto.example.mongo.flux.mongoflux.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.opencsv.CSVReader;
import com.sapto.example.mongo.flux.mongoflux.customer.persistence.Customer;
import com.sapto.example.mongo.flux.mongoflux.customer.repository.CustomerRepository;
import com.sapto.example.mongo.flux.mongoflux.employee.persistence.Employee;
import com.sapto.example.mongo.flux.mongoflux.employee.repository.EmployeeRepository;
import com.sapto.example.mongo.flux.mongoflux.order.persistence.OrderDetail;
import com.sapto.example.mongo.flux.mongoflux.order.persistence.Orders;
import com.sapto.example.mongo.flux.mongoflux.order.persistence.ShippingMethod;
import com.sapto.example.mongo.flux.mongoflux.order.repository.OrderDetailRepository;
import com.sapto.example.mongo.flux.mongoflux.order.repository.OrderRepository;
import com.sapto.example.mongo.flux.mongoflux.order.repository.ShippingMethodRepository;
import com.sapto.example.mongo.flux.mongoflux.product.persistence.Product;
import com.sapto.example.mongo.flux.mongoflux.product.repository.ProductRepository;

@Service
public class CSVProcessor implements DataProcessor {

	private static final String FAILED = "FAILED";

	private static final String OK = "ok";

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ShippingMethodRepository shippingMethodRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private OrderDetailRepository orderDetailRepository;

	public String processEmployee() {
		
		String response = FAILED;

		try {
			File file = ResourceUtils.getFile("classpath:Employees.csv");

			Reader reader = new FileReader(file);

			char[] delimeter = ";".toCharArray();
			CSVReader csvReader = new CSVReader(reader, delimeter[0]);

			int counter = 0;
			String[] nextRecord;
			try {
				while ((nextRecord = csvReader.readNext()) != null) {
					// EmployeeID;FirstName;LastName;Title;WorkPhone
				
					if (counter != 0) {
						Employee employee = new Employee(Integer.parseInt(nextRecord[0]), nextRecord[1], nextRecord[2],
								nextRecord[3], nextRecord[4]);

						employeeRepository.save(employee);
					}
					counter++;

				}
			} catch (IOException e) {
				
				e.printStackTrace();
				return FAILED;

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return FAILED;

		}

		return OK;
	}

	@Override
	public String processProduct() {
		try {
			File file = ResourceUtils.getFile("classpath:Products.csv");

			Reader reader = new FileReader(file);

			char[] delimeter = ";".toCharArray();
			CSVReader csvReader = new CSVReader(reader, delimeter[0]);

			int counter = 0;

			String[] nextRecord;
			try {
				while ((nextRecord = csvReader.readNext()) != null) {
					// ProductID;ProductName;UnitPrice;InStock
				
					if (counter != 0) {
						NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
						Number number = format.parse(nextRecord[2]);

						double unitPrice = number.doubleValue();
						Product product = new Product(Integer.parseInt(nextRecord[0]), nextRecord[1], unitPrice,
								nextRecord[3]);

						productRepository.save(product);
					}
					counter++;

				}
			} catch (IOException | ParseException e) {
				e.printStackTrace();
				return FAILED;

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return FAILED;

		}

		return OK;
	}

	@Override
	public String processCustomer() {
		try {
			File file = ResourceUtils.getFile("classpath:Customers.csv");

			Reader reader = new FileReader(file);

			char[] delimeter = ";".toCharArray();
			CSVReader csvReader = new CSVReader(reader, delimeter[0]);

			int counter = 0;

			String[] nextRecord;
			try {
				while ((nextRecord = csvReader.readNext()) != null) {
					// CustomerID;CompanyName;FirstName;LastName;BillingAddress;City;StateOrProvince;ZIPCode;Email;CompanyWebsite;PhoneNumber;FaxNumber;ShipAddress;
					// ShipCity;ShipStateOrProvince;ShipZIPCode;ShipPhoneNumber

					if (counter != 0) {
						Customer customer = new Customer(Integer.parseInt(nextRecord[0]), nextRecord[1], nextRecord[2],
								nextRecord[3], nextRecord[4], nextRecord[5], nextRecord[6], nextRecord[7],
								nextRecord[8], nextRecord[9], nextRecord[11], nextRecord[12], nextRecord[13],
								nextRecord[14], nextRecord[15], nextRecord[16]);

						customerRepository.save(customer);
					}
					counter++;

				}
			} catch (IOException e) {
				e.printStackTrace();
				return FAILED;

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return FAILED;

		}

		return OK;

	}

	@Override
	public String processShippingMethod() {
		try {
			File file = ResourceUtils.getFile("classpath:ShippingMethods.csv");

			Reader reader = new FileReader(file);

			char[] delimeter = ";".toCharArray();
			CSVReader csvReader = new CSVReader(reader, delimeter[0]);

			int counter = 0;

			String[] nextRecord;
			try {
				while ((nextRecord = csvReader.readNext()) != null) {
					// ShippingMethodID;ShippingMethod

					if (counter != 0) {

						ShippingMethod shippingMethod = new ShippingMethod(Integer.parseInt(nextRecord[0]),
								nextRecord[1]);

						shippingMethodRepository.save(shippingMethod);
					}
					counter++;

				}
			} catch (IOException e) {
				e.printStackTrace();
				return FAILED;

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return FAILED;

		}

		return OK;

	}

	@Override
	public String processOrders() {
		try {
			File file = ResourceUtils.getFile("classpath:Orders.csv");

			Reader reader = new FileReader(file);

			char[] delimeter = ";".toCharArray();
			CSVReader csvReader = new CSVReader(reader, delimeter[0]);

			int counter = 0;

			String[] nextRecord;
			try {
				while ((nextRecord = csvReader.readNext()) != null) {
					// OrderID;CustomerID;EmployeeID;OrderDate;PurchaseOrderNumber;ShipDate;ShippingMethodID;FreightCharge;Taxes;PaymentReceived;Comment

					if (counter != 0) {
						try {
							Integer id = Integer.parseInt(nextRecord[0]);
							Integer idCust = Integer.parseInt(nextRecord[1]);
							Integer idEmployee = Integer.parseInt(nextRecord[2]);
							Integer shippingMethodID = Integer.parseInt(nextRecord[6]);

							Customer customer = customerRepository.findById(idCust).orElse(new Customer());
							Employee employee = employeeRepository.findById(idEmployee).orElse(null);
							ShippingMethod shippingMethod = shippingMethodRepository.findById(shippingMethodID)
									.orElse(null);

							SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

							Date orderDate = null;
							if (!nextRecord[3].isEmpty()) {
								orderDate = formatter.parse(nextRecord[3]);
							}

							String purchaseOrderNumber = nextRecord[4];
							Date shippingDate = null;
							if (!nextRecord[5].isEmpty()) {
								shippingDate = formatter.parse(nextRecord[5]);
							}

							Integer freightCharge = null;

							if (!nextRecord[7].isEmpty()) {
								freightCharge = Integer.parseInt(nextRecord[7]);
							}

							Integer taxes = null;
							if (!nextRecord[8].isEmpty()) {
								taxes = Integer.parseInt(nextRecord[8]);
							}
							String paymentReceived = nextRecord[9];
							String comment = nextRecord[10];
							Orders order = new Orders(id, orderDate, purchaseOrderNumber, shippingDate, shippingMethod,
									employee, customer, freightCharge, taxes, paymentReceived, comment);

							orderRepository.save(order);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
					counter++;

				}
			} catch (IOException e) {
				e.printStackTrace();
				return FAILED;

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return FAILED;

		}

		return OK;
	}

	@Override
	public String processOderDetail() {
		try {
			File file = ResourceUtils.getFile("classpath:OrderDetails.csv");

			Reader reader = new FileReader(file);

			char[] delimeter = ";".toCharArray();
			CSVReader csvReader = new CSVReader(reader, delimeter[0]);

			String[] nextRecord;

			int counter = 0;

			try {
				while ((nextRecord = csvReader.readNext()) != null) {
					// OrderDetailID;OrderID;ProductID;Quantity;UnitPrice;Discount

					if (counter != 0) {
						Integer orderDetailID = Integer.parseInt(nextRecord[0]);

						Integer orderId = Integer.parseInt(nextRecord[1]);
						Integer productId = Integer.parseInt(nextRecord[2]);
						Orders order = orderRepository.findById(orderId).orElse(null);
						Product product = productRepository.findById(productId).orElse(null);

						Integer qty = Integer.parseInt(nextRecord[3]);
						NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
						Number number = format.parse(nextRecord[4]);

						Double unitPrice = number.doubleValue();
						String disctRate = nextRecord[5];
						disctRate = disctRate.replaceAll("%", "");
						Integer discount = Integer.parseInt(disctRate);

						OrderDetail orderDetail = new OrderDetail(orderDetailID, qty, unitPrice, discount, product,
								order);

						orderDetailRepository.save(orderDetail);
					}
					counter++;

				}
			} catch (IOException | ParseException e) {
				e.printStackTrace();
				return FAILED;

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return FAILED;

		}

		return OK;

	}

}
