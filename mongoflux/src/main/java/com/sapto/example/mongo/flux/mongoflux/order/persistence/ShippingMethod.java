package com.sapto.example.mongo.flux.mongoflux.order.persistence;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="shippingMethod")
public class ShippingMethod implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private Integer shippingMethodID;

	private String shippingMethodName;
	
	public ShippingMethod() {
		// TODO Auto-generated constructor stub
	}
	
	public ShippingMethod(Integer id, String shippingMethodName) {
		this.shippingMethodID =id;
		this.shippingMethodName=shippingMethodName;
	}

	public Integer getShippingMethodID() {
		return shippingMethodID;
	}

	public void setShippingMethodID(Integer shippingMethodID) {
		this.shippingMethodID = shippingMethodID;
	}

	public String getShippingMethodName() {
		return shippingMethodName;
	}

	public void setShippingMethodName(String shippingMethodName) {
		this.shippingMethodName = shippingMethodName;
	}

}
