package com.sapto.example.mongo.flux.mongoflux.order.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.sapto.example.mongo.flux.mongoflux.order.persistence.OrderDetail;
import com.sapto.example.mongo.flux.mongoflux.order.persistence.Orders;

@Repository
public interface OrderDetailRepository extends MongoRepository<OrderDetail, Integer> {

	List<OrderDetail> findByOrders(Orders orders);

}
