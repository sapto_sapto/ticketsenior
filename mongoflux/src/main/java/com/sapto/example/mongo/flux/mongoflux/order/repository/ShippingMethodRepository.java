package com.sapto.example.mongo.flux.mongoflux.order.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.sapto.example.mongo.flux.mongoflux.order.persistence.ShippingMethod;

@Repository
public interface ShippingMethodRepository extends MongoRepository<ShippingMethod, Integer> {

}
