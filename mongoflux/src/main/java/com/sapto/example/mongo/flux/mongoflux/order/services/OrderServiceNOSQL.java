package com.sapto.example.mongo.flux.mongoflux.order.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.sapto.example.mongo.flux.mongoflux.order.persistence.OrderDetail;
import com.sapto.example.mongo.flux.mongoflux.order.persistence.Orders;
import com.sapto.example.mongo.flux.mongoflux.order.persistence.OrdersManaged;
import com.sapto.example.mongo.flux.mongoflux.order.repository.OrderDetailRepository;
import com.sapto.example.mongo.flux.mongoflux.order.repository.OrderRepository;

@Service("orderServiceRDBMS")
public class OrderServiceNOSQL implements OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private OrderDetailRepository orderDetailRepository;

	@Override
	public Page<OrdersManaged> findAllOrder(PageRequest pageableRequest) {
		Page<Orders> orders = orderRepository.findAll(pageableRequest);
		List<OrdersManaged> ordersManageds = new ArrayList<>();
		for (Orders ordersUnit : orders) {
			List<OrderDetail> orderDetails = findByOrderId(ordersUnit.getOrderID());
			OrdersManaged ordersManaged = new OrdersManaged(ordersUnit, orderDetails);
			ordersManageds.add(ordersManaged);
		}

		int start = (int) pageableRequest.getOffset();
		int end = (start + pageableRequest.getPageSize()) > ordersManageds.size() ? ordersManageds.size()
				: (start + pageableRequest.getPageSize());

		Page<OrdersManaged> orderManagedPages = new PageImpl<>(ordersManageds.subList(start, end), pageableRequest,
				ordersManageds.size());
		return orderManagedPages;
	}

	public List<OrderDetail> findByOrderId(Integer id) {
		Orders orders = orderRepository.findById(id).orElse(null);
		List<OrderDetail> orderDetails = orderDetailRepository.findByOrders(orders);
		return orderDetails;
	}

}
