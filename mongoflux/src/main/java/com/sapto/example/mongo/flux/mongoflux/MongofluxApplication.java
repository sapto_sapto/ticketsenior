package com.sapto.example.mongo.flux.mongoflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongofluxApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongofluxApplication.class, args);
	}
}
