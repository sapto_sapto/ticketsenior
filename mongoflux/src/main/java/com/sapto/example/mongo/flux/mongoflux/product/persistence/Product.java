package com.sapto.example.mongo.flux.mongoflux.product.persistence;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="product")
public class Product implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private Integer productID;

	private String productName;

	private Double unitPrice;

	private String inStock;
	
	public Product() {
		// TODO Auto-generated constructor stub
	}
	
	public Product(Integer productID, String productName, Double unitPrice, String inStock) {
		this.productID = productID;
		this.productName=productName;
		this.unitPrice=unitPrice;
		this.inStock=inStock;
	}
	
	

	public Integer getProductID() {
		return productID;
	}

	public void setProductID(Integer productID) {
		this.productID = productID;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getInStock() {
		return inStock;
	}

	public void setInStock(String inStock) {
		this.inStock = inStock;
	}

}
