package com.sapto.example.mongo.flux.mongoflux.order.persistence;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.sapto.example.mongo.flux.mongoflux.product.persistence.Product;

@Document(collection="orderDetail")
public class OrderDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private Integer orderDetailID;

	
	private Integer quantity;
	
	private Double unitPrice;
	
	private Integer discount;

	private Product product;

	private Orders orders;
	
	public OrderDetail() {
	}
	

	public OrderDetail(Integer orderDetailID, Integer quantity, Double unitPrice, Integer discount, Product product,
			Orders orders) {
		super();
		this.orderDetailID = orderDetailID;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.discount = discount;
		this.product = product;
		this.orders = orders;
	}

	public Integer getOrderDetailID() {
		return orderDetailID;
	}

	public void setOrderDetailID(Integer orderDetailID) {
		this.orderDetailID = orderDetailID;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Orders getOrders() {
		return orders;
	}

	public void setOrders(Orders orders) {
		this.orders = orders;
	}

}
