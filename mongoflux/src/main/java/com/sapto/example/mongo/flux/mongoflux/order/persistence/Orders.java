package com.sapto.example.mongo.flux.mongoflux.order.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.sapto.example.mongo.flux.mongoflux.customer.persistence.Customer;
import com.sapto.example.mongo.flux.mongoflux.employee.persistence.Employee;
import com.sapto.example.mongo.flux.mongoflux.product.persistence.Product;

@Document(collection="orders")
public class Orders implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id	
	private Integer orderID;

	private Date orderDate;

	private String purchaseOrderNumber;

	private Date ShipDate;

	private ShippingMethod shippingMethod;

	private Employee employee;

	private Customer customer;

	private Integer freightCharge;
	
	private Integer taxes;
	private String paymentReceived;
	
	private String comment;

	
	public Orders() {
		// TODO Auto-generated constructor stub
	}

	public Orders(Integer orderID, Date orderDate, String purchaseOrderNumber, Date shipDate,
			ShippingMethod shippingMethod, Employee employee, Customer customer, Integer freightCharge, Integer taxes,
			String paymentReceived, String comment) {
		super();
		this.orderID = orderID;
		this.orderDate = orderDate;
		this.purchaseOrderNumber = purchaseOrderNumber;
		this.ShipDate = shipDate;
		this.shippingMethod = shippingMethod;
		this.employee = employee;
		this.customer = customer;
		this.freightCharge = freightCharge;
		this.taxes = taxes;
		this.paymentReceived = paymentReceived;
		this.comment = comment;
	}

	public Integer getOrderID() {
		return orderID;
	}

	public void setOrderID(Integer orderID) {
		this.orderID = orderID;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public Date getShipDate() {
		return ShipDate;
	}

	public void setShipDate(Date shipDate) {
		ShipDate = shipDate;
	}

	public Integer getFreightCharge() {
		return freightCharge;
	}

	public void setFreightCharge(Integer freightCharge) {
		this.freightCharge = freightCharge;
	}

	public Integer getTaxes() {
		return taxes;
	}

	public void setTaxes(Integer taxes) {
		this.taxes = taxes;
	}

	public String getPaymentReceived() {
		return paymentReceived;
	}

	public void setPaymentReceived(String paymentReceived) {
		this.paymentReceived = paymentReceived;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public ShippingMethod getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(ShippingMethod shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	// Daftar produk yang dipesan beserta kuantitas, harga satuan, diskon dan harga subtotal per item baris: nama,qty, harga, diskon, subtotal
	// Detail item pembayaran (subtotal pemesanan, pajak, biaya pengiriman dan total  biaya keseluruhan): totalharga, biaya kirim, pajak, total

	
}
