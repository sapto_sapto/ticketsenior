package com.sapto.example.mongo.flux.mongoflux.product.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.sapto.example.mongo.flux.mongoflux.product.persistence.Product;

@Repository
public interface ProductRepository extends MongoRepository<Product, Integer> {

}
