package com.sapto.example.mongo.flux.mongoflux.order.persistence;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ProductOrderInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Daftar produk yang dipesan beserta kuantitas, harga satuan, diskon dan harga
	// subtotal per item baris: nama,qty, harga, diskon, subtotal

	@JsonIgnore
	private OrderDetail orderDetail;

	private String name;

	private Integer quantity;

	private Double unitPrice;

	private Double totalPrice;
	
	private Integer discount;

	public ProductOrderInfo() {
		// TODO Auto-generated constructor stub
	}
	
	

	public ProductOrderInfo(String name, Integer quantity, Double unitPrice, Double totalPrice, Integer discount) {
		super();
		this.name = name;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.totalPrice = totalPrice;
		this.discount = discount;
	}



	public ProductOrderInfo(OrderDetail orderDetail) {
		this.orderDetail = orderDetail;
	}

	public OrderDetail getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(OrderDetail orderDetail) {
		this.orderDetail = orderDetail;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	
}
