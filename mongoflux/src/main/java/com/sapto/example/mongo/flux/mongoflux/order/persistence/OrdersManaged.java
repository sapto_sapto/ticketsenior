package com.sapto.example.mongo.flux.mongoflux.order.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.sapto.example.mongo.flux.mongoflux.product.persistence.Product;

public class OrdersManaged  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	

	private List<OrderDetail> orderDetails;
	
	//@Transient
	private Double priceBeforeTaxAndShipment;
	
	//@Transient
	private Double finalPrice;
	
	private Orders orders;
	

	//@Transient
	private List<ProductOrderInfo> productOrderInfos;

	public OrdersManaged() {
		// TODO Auto-generated constructor stub
	}
	
	public OrdersManaged(Orders orders, List<OrderDetail> orderDetails) {
		this.orders=orders;
		this.orderDetails = orderDetails;
	}

	
	

	public List<OrderDetail> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public List<ProductOrderInfo> getProductOrderInfos() {
		List<ProductOrderInfo> productOrderInfosCurrent = new ArrayList<>();
		for (OrderDetail orderDetail : orderDetails) {
			Product product = orderDetail.getProduct();

			ProductOrderInfo productOrderInfo = new ProductOrderInfo(product.getProductName(),
					orderDetail.getQuantity(), product.getUnitPrice(),
					product.getUnitPrice() * orderDetail.getQuantity(), orderDetail.getDiscount());
			productOrderInfosCurrent.add(productOrderInfo);

		}
		return productOrderInfosCurrent;
	}

	public void setProductOrderInfos(List<ProductOrderInfo> productOrderInfos) {
		this.productOrderInfos = productOrderInfos;
	}

	public Double getPriceBeforeTaxAndShipment() {
		List<ProductOrderInfo> productOrderInfos = getProductOrderInfos();
		Double priceBeforeTax = new Double(0.0);
		for (ProductOrderInfo productOrderInfo : productOrderInfos) {
			priceBeforeTax += productOrderInfo.getTotalPrice();
		}
		
		
		
		
		return priceBeforeTax;
	}

	public void setPriceBeforeTaxAndShipment(Double priceBeforeTaxAndShipment) {
		this.priceBeforeTaxAndShipment = priceBeforeTaxAndShipment;
	}

	public Double getFinalPrice() {
		Double totalPrice = getPriceBeforeTaxAndShipment();
		Integer deliveryFee = orders.getFreightCharge();
		if(deliveryFee==null) {
			deliveryFee=0;
		}
		totalPrice = totalPrice+deliveryFee+orders.getTaxes();
		return totalPrice;
	}

	public void setFinalPrice(Double finalPrice) {
		this.finalPrice = finalPrice;
	}

	public Orders getOrders() {
		return orders;
	}

	public void setOrders(Orders orders) {
		this.orders = orders;
	}

	// Daftar produk yang dipesan beserta kuantitas, harga satuan, diskon dan harga subtotal per item baris: nama,qty, harga, diskon, subtotal
	// Detail item pembayaran (subtotal pemesanan, pajak, biaya pengiriman dan total  biaya keseluruhan): totalharga, biaya kirim, pajak, total

	
}
