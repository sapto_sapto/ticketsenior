package com.sapto.example.mongo.flux.mongoflux.employee.persistence;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.sapto.example.mongo.flux.mongoflux.persistence.BaseUser;
import com.sapto.example.mongo.flux.mongoflux.persistence.IBaseUser;

@Document(collection="employee")
public class Employee extends BaseUser implements IBaseUser, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private Integer employeeID;

	private String title;
	private String workPhone;
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}
	
	public Employee(Integer employeeID, String firstName, String lastName, String title, String workPhone) {
		this.employeeID = employeeID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.title = title;
		this.workPhone = workPhone;
	}

	public Integer getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

}
