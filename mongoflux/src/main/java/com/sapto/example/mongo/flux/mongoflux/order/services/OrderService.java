package com.sapto.example.mongo.flux.mongoflux.order.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.sapto.example.mongo.flux.mongoflux.order.persistence.OrderDetail;
import com.sapto.example.mongo.flux.mongoflux.order.persistence.Orders;
import com.sapto.example.mongo.flux.mongoflux.order.persistence.OrdersManaged;

public interface OrderService {

	Page<OrdersManaged> findAllOrder(PageRequest pageReq);

	List<OrderDetail> findByOrderId(Integer id);

}
