package com.sapto.example.mongo.flux.mongoflux.employee.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.sapto.example.mongo.flux.mongoflux.employee.persistence.Employee;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, Integer> {

}
