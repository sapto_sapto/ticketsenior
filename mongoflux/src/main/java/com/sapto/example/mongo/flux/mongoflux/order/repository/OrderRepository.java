package com.sapto.example.mongo.flux.mongoflux.order.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.sapto.example.mongo.flux.mongoflux.order.persistence.Orders;

public interface OrderRepository extends MongoRepository<Orders, Integer>{

}
